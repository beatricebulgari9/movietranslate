<?php
   $DOCUMENT_ROOT = $_SERVER["DOCUMENT_ROOT"];
   include($DOCUMENT_ROOT.'/functions.php');

   $search_GET = filter_input(INPUT_GET, 'search') ?? '';
   $search_GET = strtolower($search_GET);
   $page_GET = filter_input(INPUT_GET, 'page') ?? '1';

   require_once($DOCUMENT_ROOT.'/index.php');
