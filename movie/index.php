<?php
   $DOCUMENT_ROOT = $_SERVER["DOCUMENT_ROOT"];
   $SETTINGS = parse_ini_file($DOCUMENT_ROOT.'/settings.ini.php');
   $SCRIPT_ROOT = $SETTINGS['SCRIPT_ROOT'];

   // functions
   include($DOCUMENT_ROOT.'/functions.php');

   $api_key = $SETTINGS['api_key'];

   // json
   $countries = json_decode(file_get_contents($DOCUMENT_ROOT.'/json/countries.json'), true);
   $languages = json_decode(file_get_contents($DOCUMENT_ROOT.'/json/languages.json'), true);

   $movie_id = filter_input(INPUT_GET, 'id');
   
   if (empty($movie_id))
   {
      die('Movie not found');
   }

   // alternative titles
   $url = "https://api.themoviedb.org/3/movie/".$movie_id."/translations";
   $parameters = [
      "api_key" => $api_key,
   ];

   $alternative_titles = api_call("GET", $url, $parameters);
   $alternative_titles = json_decode($alternative_titles, true);

   // saving all iso 639-1 laguages languages available for this movie
   // to get later the trailer
   $iso_languages = [];
   foreach ($alternative_titles["translations"] as $alternative_title)
   {
      if (!in_array($alternative_title["iso_639_1"], $iso_languages))
      {
         $iso_languages[] = $alternative_title["iso_639_1"];
      }
   }

   // client language
   $lang = substr($_SERVER['HTTP_ACCEPT_LANGUAGE'], 0, 2);

   $url = "https://api.themoviedb.org/3/movie/".$movie_id;
   $parameters = [
      "api_key" => $api_key,
   ];

   $movie_details = api_call("GET", $url, $parameters);
   $movie_details = json_decode($movie_details, true);

   $url = "https://api.themoviedb.org/3/movie/".$movie_id."/recommendations";
   $parameters = [
      "api_key" => $api_key
   ];

   $recommended_movies = api_call("GET", $url, $parameters);
   $recommended_movies = json_decode($recommended_movies, true);

   $url = "https://api.themoviedb.org/3/movie/".$movie_id."/videos";
   $parameters = [
      "api_key" => $api_key
   ];

   $movie_videos = api_call("GET", $url, $parameters);
   $movie_videos = json_decode($movie_videos, true);

   $video_key = "";
   $teaser_key = "";
   $default_trailer_language = "";
   $default_teaser_language = "";

   foreach ($movie_videos["results"] as $video)
   {
      if (isset($video["type"]))
      {
         if ($video["type"] == "Trailer" && $video["site"] == "YouTube")
         {
            $default_trailer_language = $video["iso_639_1"];
            $video_key = $video["key"];
            break;
         }

         if ($video["type"] == "Teaser" && $video["site"] == "YouTube")
         {
            $default_teaser_language = $video["iso_639_1"];
            $teaser_key = $video["key"];
         }
      }
   }

   // if there is no trailer, take the teaser if available
   if (empty($video_key))
   {
      $default_trailer_language = $default_teaser_language;
      $video_key = $teaser_key;
   }

   $n_languages = 0;
   foreach ($alternative_titles["translations"] as $alternative_title)
   {
      if (!empty($alternative_title["data"]["title"]))
      {
         $n_languages++;
      }
   }
?>
<!DOCTYPE html>
<html lang="en">

   <head>
      
      <!-- favicon -->
      <link rel="apple-touch-icon" sizes="180x180" href="<?=$SCRIPT_ROOT?>/apple-touch-icon.png">
      <link rel="icon" type="image/png" sizes="32x32" href="<?=$SCRIPT_ROOT?>/favicon/favicon-32x32.png">
      <link rel="icon" type="image/png" sizes="16x16" href="<?=$SCRIPT_ROOT?>/favicon/favicon-16x16.png">
      <link rel="mask-icon" href="<?=$SCRIPT_ROOT?>/safari-pinned-tab.svg" color="#5bbad5">
      <link rel="shortcut icon" href="<?=$SCRIPT_ROOT?>/favicon/favicon.ico">
      <meta name="msapplication-TileColor" content="#2d89ef">
      <meta name="msapplication-TileImage" content="<?=$SCRIPT_ROOT?>/favicon/mstile-144x144.png">
      <meta name="msapplication-config" content="<?=$SCRIPT_ROOT?>/favicon/browserconfig.xml">
      <meta name="theme-color" content="#ffffff">
      <!-- /favicon -->

      <meta charset="UTF-8">
      <title>MT - <?php echo $movie_details["title"] ?> <?php echo ($movie_details["title"] != $movie_details["original_title"]) ? "(".$movie_details["original_title"].")":"" ?> | MovieTranslate</title>
      <?php
         $description_languages = "Discover how the movie ".$movie_details["title"]." was translated arround the world and take a look at movie trailers in different languages. The movie ".$movie_details["title"]." was translated in ".$n_languages." languages.";
         
         if (count($alternative_titles["translations"]) > 0)
         {
            $description_languages .= " Some of the languages are ";
         }
         
         foreach ($alternative_titles["translations"] as $alternative_title)
         {
            if (strlen($description_languages) >= 310) 
            {
               $description_languages .= "...";
               break;
            }
            
            $description_languages .= $alternative_title["name"].", ";
         }
         $description_languages = rtrim($description_languages, ', ');
      ?>
      <link href="https://fonts.googleapis.com/css2?family=Cormorant+Garamond:wght@400;700&family=Roboto:wght@300;500&display=swap" rel="stylesheet">
      <link rel="stylesheet" type="text/css" href="./css/main.css?v=<?=rand(1, 10000000)?>">
      <meta name="description" content="<?php echo $description_languages?>">
   </head>

   <body>

      <div class="container" id="movie_container" data-movie_id="<?php echo $movie_id ?>">

         <!-- logo, slogan -->
         <div class="logo_container">
            <a href="<?=$SCRIPT_ROOT?>/index.php">
               <div class="logo"><span>M</span>ovie<span>T</span>ranslate</div>
               <div class="detailed">Alternative titles for movies released arround the world</div>
            </a>
         </div>
         <!-- /logo, slogan -->

         <!-- movie informations -->
         <div class="movie_informations">

            <!-- header -->
            <div class="header">
               <div class="left"></div>
               <div class="right">
                  <img class="poster_mobile" src="" alt="<?php echo $movie_details["title"] ?> poster " title="<?php echo $movie_details["title"] ?> poster"/>
                  <div class="title_informations">
                     <div class="title"><h1><?php echo $movie_details["title"] ?> (<?php echo str_replace('-', '.', $movie_details["release_date"]) ?>)</h1></div>
                     <?php
                        if ($movie_details["title"] != $movie_details["original_title"])
                        {
                           echo '<div class="original_title" title="Original release movie title"><h2>'.$movie_details["original_title"].'</h2></div>';
                        }
                     ?>
                  </div>
               </div>
            </div>
            <!-- /header -->

            <!-- left block -->
            <div class="left">
               <?php
                  $get_headers = get_headers('https://image.tmdb.org/t/p/original/'.$movie_details["poster_path"]);
                  if (($get_headers[0] == 'HTTP/1.1 200 OK' || $get_headers[0] == 'HTTP/1.0 301 Moved Permanently') && $get_headers[8] != 'HTTP/1.1 403 Forbidden')
                  {
                     ?>
                     <img class="poster" 
                        src="https://image.tmdb.org/t/p/original/<?=$movie_details["poster_path"]?>" 
                        alt="<?php echo $movie_details["title"] ?> poster" 
                        title="<?php echo $movie_details["title"] ?> poster"
                        <?php echo empty($video_key) ? 'style="margin-top: 20px;"':'' ?>/>
                     <?php
                  }
                  else
                  {
                     ?>
                     <div class="poster poster_not_found"></div>
                     <?php
                  }
               ?>
            </div>
            <!-- /left block -->

            <!-- right block -->
            <div class="right">
               <?php
                  if (!empty($video_key))
                  {
                     $trailer_table = '<table class="movie_trailer">';
                        $trailer_table .= '<tr>';
                           $trailer_table .= '<th>Trailer</th>';
                           $trailer_table .= '<th class="select_trailer">';
                              $trailer_table .= '<select id="movie_trailer_language">';
                                 $selected = "";
                                 foreach ($iso_languages as $iso_language)
                                 {
                                    if ($iso_language == $default_trailer_language)
                                    {
                                       $selected = "selected";
                                    }
                                    else
                                    {
                                       $selected = "";
                                    }

                                    if (isset($languages[$iso_language]["nativeName"]))
                                    {
                                       $language_translated = $languages[$iso_language]["nativeName"];
                                       $trailer_table .= '<option value="'.$iso_language.'" '.$selected.'>'.$language_translated.'</option>';
                                    }
                                 }
                              $trailer_table .= '</select>';
                           $trailer_table .= '</th>';
                        $trailer_table .= '</tr>';
                        $trailer_table .= '<tr class="trailer" data-iso_language="'.$default_trailer_language.'">';
                           $trailer_table .= '<td colspan="2"><iframe width="600" height="300" src="https://www.youtube-nocookie.com/embed/'.$video_key.'" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe></td>';
                        $trailer_table .= '</tr>';
                        $trailer_table .= '<tr class="tr_youtube_link" data-iso_language="'.$default_trailer_language.'">';
                           $trailer_table .= '<td colspan="2" class="youtube_link"><a href="https://www.youtube.com/watch?v='.$video_key.'" target="_blank">Watch on YouTube</a></td>';
                        $trailer_table .= '</tr>';
                     $trailer_table .= '</table>';

                     echo $trailer_table;
                  }
               ?>
               <table class="movie_details">
                  <tr>
                     <td>
                        <label>Original Language</label>
                        <span>
                           <?php 
                              if (isset($languages[$movie_details["original_language"]]))
                              {
                                 echo str_replace('; ', ' - ', $languages[$movie_details["original_language"]]["name"]);
                              }
                              else
                              {
                                 echo "-";
                              }
                           ?>
                        </span>
                     </td>
                     <td>
                        <label>Spoken Languages</label>
                        <span>
                           <?php 
                              foreach ($movie_details["spoken_languages"] as $i => $spoken_language)
                              {
                                 if ($i == 0)
                                    echo $spoken_language["name"];
                                 else
                                    echo ", ".$spoken_language["name"];
                              }
                           ?>
                        </span>
                     </td>
                     <td>
                        <label>Title translated in</label>
                        <span>
                           <?php
                              echo $n_languages. " languages";
                           ?>
                        </span>
                     </td>
                  </tr>
                  <tr>
                     <td>
                        <label>Budget</label>
                        <?php 
                           if (!empty($movie_details["budget"]))
                           {
                              echo "<span>$ ".number_format($movie_details["budget"], 0, '.', "'")."</span>";
                           }
                           else
                           {
                              echo "<span class='not_found'>n.d</span>";
                           } 
                        ?>
                     </td>
                     <td>
                        <label>Revenue</label>
                        <?php 
                           if (!empty($movie_details["revenue"]))
                           {
                              echo "<span>$ ".number_format($movie_details["revenue"], 0, '.', "'")."</span>";
                           }
                           else
                           {
                              echo "<span class='not_found'>n.d</span>";
                           } 
                        ?>
                     </td>
                     <td>
                        <label>Popularity</label>
                        <span><?php echo round($movie_details["popularity"], 2) ?></span>
                     </td>
                  </tr>
               </table>
               <table class="release_table">
                  <tr class="header">
                     <th id="title_th" class="t_l noselect">Title</th>
                     <th id="language_th" class="t_r noselect">Language</th>
                     <th class="t_r" style="border:0"><input class="search_region" type="text" placeholder="Region" title="Click and search your Region"></th>
                  </tr>
                  <?php
                     $append = "";
                     foreach ($alternative_titles["translations"] as $alternative_title)
                     {
                        $tr_title = "The movie ".$movie_details["title"]." was released in ".((isset($countries[$alternative_title["iso_3166_1"]])) ? $countries[$alternative_title["iso_3166_1"]]["name"]:"")." under ". $alternative_title["data"]["title"] ." title. ". $alternative_title["data"]["title"] ." trailer";
                        if (!empty($alternative_title["data"]["title"]))
                        {
                           $append .= "<tr title='".$tr_title."' data-iso_language='".strtolower($alternative_title["iso_3166_1"])."'>";
                              $append .= "<td class='t_l alternative_title' readonly='readonly'>";
                                 $append .= $alternative_title["data"]["title"];
                              $append .= "</td>";
                              $append .= "<td class='t_r movie_language'>";
                                 if (empty($alternative_title["name"]))
                                 {
                                    $append .= '<span>n.d</span>';
                                 }
                                 else
                                 {
                                    $append .= $alternative_title["name"];
                                 }
                              $append .= "</td>";
                              $append .= "<td class='t_r movie_region'>";
                                 if (isset($countries[$alternative_title["iso_3166_1"]]))
                                    $append .= $countries[$alternative_title["iso_3166_1"]]["native"];
                              $append .= "</td>";
                           $append .= "</tr>";
                        }
                     }

                     $tr_title = $movie_details["original_title"]." trailer";
                     $append .= "<tr title='test' data-iso_language='".strtolower($movie_details["original_language"])."'>";
                        $append .= "<td class='t_l alternative_title' readonly='readonly'>";
                           $append .= $movie_details["original_title"];
                        $append .= "</td>";
                        $append .= "<td class='t_r'>";
                           if (isset($languages[$movie_details["original_language"]]))
                           {
                              $append .= str_replace('; ', ' - ', $languages[$movie_details["original_language"]]["name"]);
                           }
                           else
                           {
                              $append .= '<span>n.d</span>';
                           }
                        $append .= "</td>";
                        $append .= "<td class='t_r movie_region'>";
                           if (!empty($movie_details["production_countries"]))
                           {
                              if (isset($countries[$movie_details["production_countries"][0]["iso_3166_1"]]["native"]))
                              {
                                 $append .= $countries[$movie_details["production_countries"][0]["iso_3166_1"]]["native"];
                              }
                           }
                        $append .= "</td>";
                     $append .= "</tr>";

                     echo $append;

                  ?>
               </table>
               <table class="recommended_movies">
                  <tr class="header">
                     <th colspan="5" class="t_l noselect">Movies like <?php echo $movie_details["title"] ?></th>
                  </tr>
                  <?php
                     $tr_switcher = 0;
                     $mobile = isMobileDevice();
                     $available_movies = false;

                     $counter = 0;
                     foreach ($recommended_movies["results"] as $recommended_movie)
                     {
                        if ($counter >= 10)
                        {
                           break;
                        }

                        if (!$available_movies) $available_movies = true;

                        if ($tr_switcher == 0) echo "<tr>";

                        $tr_switcher++;

                        if ($tr_switcher == 5)
                        {
                           $temp_style = 'style="right:0"';
                        }
                        else
                        {
                           $temp_style = '';
                        }

                        echo "<td>";
                           echo '<a href="'.$SCRIPT_ROOT.'/movie/index.php?id='.$recommended_movie["id"].'">';
                              echo '<img class="recommended_poster" ';
                                 echo 'onerror="this.classList.add(\'poster_not_found\')" ';
                                 echo 'src="https://image.tmdb.org/t/p/original/'.$recommended_movie['poster_path'].'" ';
                                 echo 'alt="'.$recommended_movie["title"].' poster" ';
                                 echo 'title="'.$recommended_movie["title"].' poster"/>';

                                 if (isset($recommended_movie["release_date"]) && !empty($temp_release_date))
                                 {
                                    $temp_release_date = substr($temp_release_date, 0, 4);
                                 }
                                 else
                                 {
                                    $temp_release_date = "n.d";
                                 }

                                 echo '<div class="recommended_overlay" '.$temp_style.'><div>'.$recommended_movie["title"].'</div><span>'.$temp_release_date.'</span></div>';
                           echo '</a>';
                        echo "</td>";

                        if ($tr_switcher == 5)
                        {
                           echo "</tr>";
                           $tr_switcher = 0;
                        }

                        $counter++;
                     }

                     if (!$available_movies)
                     {
                        echo "<tr>";
                           echo "<td class='not_found no_reccomendations'>There are no similar movies to ".$movie_details["title"] ."</td>";
                        echo "</tr>";
                     }
                  ?>
               </table>
            </div>
            <!-- /right block -->

         </div>
         <!-- /movie informations -->

      </div>

      <!-- footer -->
      <div class="footer">
         Copyright <span id="copyright_year"></span> 
         - All Rights Reserved. This product uses the TMDb API but is not endorsed or certified by TMDb.
      </div>
      <!-- /footer -->

      <!-- scripts -->
      <script src="<?=$SCRIPT_ROOT?>/js/jquery-3.6.1.min.js"></script>
      <script src="<?=$SCRIPT_ROOT?>/movie/js/sort.js"></script>
      <script src="<?=$SCRIPT_ROOT?>/movie/js/main.js?v=<?=rand(1, 10000000)?>"></script>
      <!-- /scripts -->

   </body>
</html>
