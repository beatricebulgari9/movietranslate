$(function()
{
   $('#copyright_year').html(new Date().getFullYear());

   // region search
   $('.search_region').on('input', function()
   {
      var value = $(this).val().toLowerCase();

      var found_titles = 0;
      $('.release_table tr').each(function()
      {
         if ($(this).hasClass('header') || $(this).hasClass('no_region')) return true;

         var  movie_region = $(this).find('.movie_region').html().toLowerCase();
         
         if (!movie_region.includes(value))
         {
            $(this).hide();
         }
         else
         {
            found_titles++;
            $(this).show();
         }
      });

      if (found_titles == 0)
      {
         $('.release_table').append('<tr class="not_found"><td colspan="3">Movie doesn\'t exists in searched region</td></tr>');
      }
      else
      {
         $('.not_found').remove();
      }
   });

   var table = $('.release_table');
   $('#language_th, #title_th').each(function()
   {
      var th = $(this), thIndex = th.index(), inverse = false;
      th.click(function()
      {
         table.find('td').filter(function()
         {
            return $(this).index() === thIndex;

         }).sortElements(function(a, b)
         {
            if( $.text([a]) == $.text([b]) )
               return 0;

            return $.text([a]) > $.text([b]) ?
               inverse ? -1 : 1
               : inverse ? 1 : -1;

         }, 
         function()
         {
            return this.parentNode; 
         });

         if (inverse)
         {
            $('.th_order').remove();
            if ($(this).attr('id') == "title_th")
            {
               $(this).append(' <span class="th_order">Desc</span>');
            }
            else
            {
               $(this).prepend(' <span class="th_order">Desc</span>');
            }
         }
         else
         {
            $('.th_order').remove();

            if ($(this).attr('id') == "title_th")
            {
               $(this).append(' <span class="th_order">Asc</span>');
            }
            else
            {
               $(this).prepend(' <span class="th_order">Asc</span>');
            }
         }

         inverse = !inverse;
      });
   });

   $('.search_region').focus(function()
   {
      if ($(this).text() == "")
      {
         $(this).attr('placeholder', "");
      }
   });
   
   $('.search_region').focusout(function()
   {
      if ($(this).text() == "")
      {
         $(this).attr('placeholder', "Region");
      }
   });

   $('#share_button').click(function()
   {
      var input = document.getElementById("share_input");
      input.select();
      input.setSelectionRange(0, 99999);
      document.execCommand("copy");

      $(this).animate({'background-color': '#51ffa7'}, 'slow');
   });

   $('#movie_trailer_language').change(function()
   {
      var selected_language = $(this).val();
      var id = $('#movie_container').attr('data-movie_id');

      if ($('.trailer[data-iso_language="'+selected_language+'"]').length > 0)
      {
         $('.trailer').hide();
         $('.trailer[data-iso_language="'+selected_language+'"]').show();

         $('.tr_youtube_link').hide();
         $('.tr_youtube_link[data-iso_language="'+selected_language+'"]').show();
      }
      else
      {
         $.post('../movie/handlers/handler.php', {
            action: 1,
            id: id,
            language: selected_language
         })
         .done(function(response)
         {
            response = $.parseJSON(response);
            if (response.status == "ok")
            {
               if (response.data == "n.a")
               {
                  var new_tr = '<tr class="trailer" data-iso_language="'+selected_language+'">';
                     new_tr += '<td colspan="2" class="trailer_not_found">';

                        var temp_title = $('.release_table tr[data-iso_language="'+selected_language+'"] .alternative_title').text();

                        new_tr += 'Trailer not found, check search results on <a class="link" href="https://www.youtube.com/results?search_query='+temp_title+'+trailer" target="_blank">Youtube</a>';
                     new_tr += '</td>';
                  new_tr += '</tr>';

                  new_tr += '<tr class="tr_youtube_link" data-iso_language="'+selected_language+'">';
                     new_tr += '<td colspan="2"></td>';
                  new_tr += '</tr>';

                  $('.movie_trailer').append(new_tr);
               }
               else
               {
                  var new_tr = '<tr class="trailer" data-iso_language="'+selected_language+'">';
                     new_tr += '<td colspan="2">';
                        new_tr += '<iframe width="600" height="300" src="https://www.youtube-nocookie.com/embed/'+response.data+'" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>';
                     new_tr += '</td>';
                  new_tr += '</tr>';

                  new_tr += '<tr class="tr_youtube_link" data-iso_language="'+selected_language+'">';
                     new_tr += '<td colspan="2" class="youtube_link">';
                        new_tr += '<a href="https://www.youtube.com/watch?v='+response.data+'" target="_blank">Watch on YouTube</a>';
                     new_tr += '</td>';
                  new_tr += '</tr>';
   
                  $('.movie_trailer').append(new_tr);
               }

               $('.trailer').hide();
               $('.trailer[data-iso_language="'+selected_language+'"]').show();

               $('.tr_youtube_link').hide();
               $('.tr_youtube_link[data-iso_language="'+selected_language+'"]').show();
            }
         });
      }

      $('.release_table td').removeClass('selected_trailer');
      $('.release_table tr[data-iso_language="'+selected_language+'"] td').addClass('selected_trailer');
   });

   // mobile image
   if ($(window).width() <= 826)
   {
      if ($('.poster_mobile').attr('src') == "")
      {
         var imageUrl = $('.poster').attr('src');
         $('.poster_mobile').attr('src', imageUrl);
      }
   }

   $(window).resize(function()
   {
      if ($(window).width() <= 826)
      {
         if ($('.poster_mobile').attr('src') == "")
         {
            var imageUrl = $('.poster').attr('src');
            $('.poster_mobile').attr('src', imageUrl);
         }
      }
   });
});
