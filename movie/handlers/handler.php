<?php
   ini_set('display_errors', 1);
   ini_set('display_startup_errors', 1);
   error_reporting(E_ALL);

   include('../../functions.php');

   $action = filter_input(INPUT_POST, 'action');

   $api_key = "395d041f95c63722560dd9746ddfbda4";

   if (empty($action))
   {
      die(json_encode([
         "status" => "ko",
         "message" => "Problems with passed parameters, try again."
      ]));
   }

   switch ($action)
   {
      case 1: // Change trailer language
         $selected_language = filter_input(INPUT_POST, 'language');
         $movie_id = filter_input(INPUT_POST, 'id');

         $data = "n.a";

         $url = "https://api.themoviedb.org/3/movie/".$movie_id."/videos";
         $parameters = [
            "api_key" => $api_key,
            "language" => $selected_language
         ];

         $movie_videos = api_call("GET", $url, $parameters);
         $movie_videos = json_decode($movie_videos, true);

         if (!empty($movie_videos["results"]))
         {
            $video_key = "";
            foreach ($movie_videos["results"] as $video)
            {
               if ($video["type"] == "Trailer" && $video["site"] == "YouTube")
               {
                  $data = $video["key"];
                  break;
               }
            }
         }
         break;
   }

   die(json_encode([
      "status" => "ok",
      "data" => $data
   ]));
