<!DOCTYPE html>
<?php
   $DOCUMENT_ROOT = $_SERVER["DOCUMENT_ROOT"];
   $SETTINGS = parse_ini_file($DOCUMENT_ROOT.'/settings.ini.php');
   $SCRIPT_ROOT = $SETTINGS['SCRIPT_ROOT'];
   
   $head_title = '';
   $head_description = '';
   if (!empty($search_GET))
   {
      $search_GET = strtolower($search_GET);
      $head_title = ' - Results for "'.$search_GET.'"';
      $head_description = 'Search results for '.$search_GET.'. Find Movie titles translated in your language';
   }
?>
<html lang="en">

   <head>

      <!-- favicon -->
      <link rel="apple-touch-icon" sizes="180x180" href="<?=$SCRIPT_ROOT?>/favicon/apple-touch-icon.png">
      <link rel="icon" type="image/png" sizes="32x32" href="<?=$SCRIPT_ROOT?>/favicon/favicon-32x32.png">
      <link rel="icon" type="image/png" sizes="16x16" href="<?=$SCRIPT_ROOT?>/favicon/favicon-16x16.png">
      <link rel="mask-icon" href="<?=$SCRIPT_ROOT?>/favicon/safari-pinned-tab.svg" color="#5bbad5">
      <link rel="shortcut icon" href="<?=$SCRIPT_ROOT?>/favicon/favicon.ico">
      <meta name="msapplication-TileColor" content="#2d89ef">
      <meta name="msapplication-TileImage" content="<?=$SCRIPT_ROOT?>/favicon/mstile-144x144.png">
      <meta name="msapplication-config" content="<?=$SCRIPT_ROOT?>/favicon/browserconfig.xml">
      <meta name="theme-color" content="#ffffff">
      <!-- /favicon -->
      
      <meta charset="UTF-8">
      <title>MovieTranslate <?=$head_title?></title>
      <meta name="description" content="<?=$head_description?>">
      <link rel="stylesheet" type="text/css" href="<?=$SCRIPT_ROOT?>/css/main.css">
      <link href="https://fonts.googleapis.com/css2?family=Cormorant+Garamond:wght@400;700&family=Roboto:wght@300;500&display=swap" rel="stylesheet">

   </head>

   <body>
      <div class="container">

         <!-- logo, slogan -->
         <div class="logo_container">

            <a href="<?=$SCRIPT_ROOT?>/index.php">
               <div class="logo"><span>M</span>ovie<span>T</span>ranslate</div>
               <div class="detailed">Alternative titles for movies released arround the world</div>
            </a>

         </div>
         <!-- /logo, slogan -->

         <!-- search -->
         <div class="search_container">
            <form id="search_form" action="./search" name="" method="GET" role="search">
               <input id="search" class="search_input" name="search" type="text" placeholder="Search by title in any language, 2 chars suggestions" autofocus autocomplete="false" value="<?=$search_GET?>"/>
               <input id="page" name="page" type="hidden" value="<?=$page_GET?>"/>
               <input id="search_button" class="search_button" aria-label="Search" type="submit" value="Search"/>
            </form>
         </div>
         <!-- /search -->

         <!-- search results container -->
         <div class="search_results"></div>

      </div>

      <!-- container for poster zoom -->
      <div class="poster_zoom"></div>

      <!-- footer -->
      <div class="footer">
         Copyright <span id="copyright_year"></span> 
         - All Rights Reserved. This product uses the TMDb API but is not endorsed or certified by TMDb.
      </div>
      <!-- /footer -->

      <!-- scripts -->
      <script>
         var SCRIPT_ROOT = "<?=$SCRIPT_ROOT?>";
         var title = "<?=$search_GET?>";
         var page = "<?=$page_GET?>";
      </script>
      <script src="<?=$SCRIPT_ROOT?>/js/jquery-3.6.1.min.js"></script>
      <script src="<?=$SCRIPT_ROOT?>/js/main.js?v=<?=rand(1, 10000000)?>"></script>
      <!-- /scripts -->
      
   </body>

</html>
