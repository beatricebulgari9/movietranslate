$(function()
{
   $('#copyright_year').html(new Date().getFullYear());

   $('#search').on('input', input_delay(function (e)
   {
      var $this = $(this);
      var searched_title = $this.val();
      var sanit_searched_name = searched_title.toLowerCase();

      if (sanit_searched_name.length < 2) return false;
   
      extendBox(sanit_searched_name);
   }, 500));


   $('#search_form').on('submit', function(e)
   {
      e.preventDefault();

      var searched_text = $('#search').val();
      var page = $('#page').val();
      
      window.location.href = SCRIPT_ROOT+'/search/index.php?search='+searched_text+'&page='+page;
   });

   if ($('#search').length > 0 && $('#search').val().length > 0)
   {
      var sanit_searched_name = title.toLowerCase();
      extendBox(sanit_searched_name);

      search_by_title(title, page);
   }
});

function search_by_title(title, page)
{
   $.get(SCRIPT_ROOT+'/handlers/search.php',
   {
      value: title,
      page: page
   })
   .done(function(response)
   {
      response = $.parseJSON(response);

      if (response.status == "ok")
      {
         var append = '<table>';
         append += '<tr>';
            append += '<th></th>';
            append += '<th class="t_l">Title</th>';
            append += '<th class="t_c">Year</th>';
            append += '<th class="t_r">Original Title</th>';
            append += '<th class="t_r">Original Language</th>';
         append += '</tr>';

         $.getJSON(SCRIPT_ROOT+"/json/languages.json", function(languages) 
         {
            if (response.data.length == 0)
            {
               // resetting append variable 
               append = '<table>';
               append += '<tr><td colspan="5" class="no_results">No results found for "'+title+'"</td></tr>';
            }
            else
            {
               for (data of response.data.results)
               {
                  var resp_title = data.title;
                  resp_title = resp_title.replace( new RegExp('('+title+')', 'ig'), "<span>$1</span>");
   
                  append += '<tr>';
                     append += '<td>';
                        append += '<a href="'+SCRIPT_ROOT+'/movie/index.php?id='+data.id+'">';
                           if (data.poster_path != "" && data.poster_path != null)
                           {
                              data.poster_path = data.poster_path.replace('/', '');
                              append += '<img src="https://image.tmdb.org/t/p/original/'+data.poster_path+'" class="poster" height="34"/>';
                           }
                           else
                           {
                              append += '<div class="noposter noselect"><span>no poster</span></div>';
                           }
                        append += '</a>';
                     append += '</td>';
                     append += '<td class="t_l link_btn title" title="Check under which title the movie '+data.title+' was released arround the world">';
                        append += '<a href="'+SCRIPT_ROOT+'/movie/index.php?id='+data.id+'">';
                           append += resp_title;
                        append += '</a>';
                     append += '</td>';
                     
                     var year = "";
                     if (data.release_date == null || data.release_date == "")
                     {
                        year = '<td class="t_c not_found"><a href="'+SCRIPT_ROOT+'/movie/index.php?id='+data.id+'">n.d</a></td>';
                     }
                     else
                     {
                        year = '<td class="t_c release_date"><a href="'+SCRIPT_ROOT+'/movie/index.php?id='+data.id+'">'+data.release_date.replace(/\-/g, '.');+'</a></td>';
                     }

                     append += year;
   
                     var language = data.original_language;
                     $.each(languages, function(k, v)
                     {
                        if (k == language)
                        {
                           language = v.nativeName;
                           return false;
                        }
                     });
                     language = language.toLowerCase();

                     // append += '<td class="t_c">'+data.language+'</td>';
                     append += '<td class="t_r">';
                        append += '<a href="'+SCRIPT_ROOT+'/movie/index.php?id='+data.id+'">';
                           append += '<span title="Original Title of the movie">'+data.original_title+'</span>';
                        append += '</a>';
                     append += '</td>';

                     append += '<td class="t_r language"><a href="'+SCRIPT_ROOT+'/movie/index.php?id='+data.id+'">'+language+'</a></td>';
                  append += '</tr>';
               }
            }
            append += '</table>';

            append += '<table>';
               if (response.data.results.length == response.data.total_results)
               {
                  append += '<tr>';
                     append += '<td class="showing_results" colspan="5">Showing '+response.data.results.length+' of '+response.data.total_results+' results</td>';
                  append += '</tr>';
               }
               else
               {
                  append += '<tr>';
                     append += '<td class="showing_results" colspan="4">Showing '+20 * (response.data.page - 1)+' - '+ (20 * (response.data.page - 1) + response.data.results.length)+' of '+response.data.total_results+' results</td>';

                     append += '<td class="t_r pages">';

                     for (var i = 0; i < response.data.total_pages; i++)
                     {
                        if (response.data.page == (i + 1))
                        {
                           append += '<span class="current_page">'+(i+1)+'</span> ';
                        }
                        else
                        {
                           append += '<span class="change_page" data-number="'+(i+1)+'">'+(i+1)+'</span> ';
                        }
                     }
                     append += '</td>';

                  append += '</tr>';
               }
            append += '</table>';

            $('.search_results').html("");
            $('.search_results').append(append);

            $('.poster').hover(function()
            {
               $('.poster_zoom').show();
               var img_src = $(this).attr('src').replace('h34_', 'h400_');
               $('.poster_zoom').html('<img src="'+img_src+'"/>');
               $('.poster').mousemove(function(e)
               {
                  $('.poster_zoom').css('left',e.pageX+"px");

                  if (e.pageY > ($(window).height() / 2))
                     $('.poster_zoom').css('top',(e.pageY - 200)+"px");
                  else
                     $('.poster_zoom').css('top',e.pageY+"px");
               });
            }, function()
            {
               $('.poster_zoom').hide();
            });
         });
      }
   });
}

$(document).on('click', '.change_page', function()
{
   var n_page = $(this).attr('data-number');
   search_by_title(title, n_page);

   $('#page').val(n_page);
});

function input_delay(fn, ms)
{
   let timer = 0
   return function(...args)
   {
      clearTimeout(timer)
      timer = setTimeout(fn.bind(this, ...args), ms || 0)
   }
}

function extendBox(sanit_searched_name)
{
   var $this = $('#search');

   // if the box is not exteded, extend it
   if (!$this.hasClass('extended'))
   {
      if ($this.val() == "")
      {
         // ... return to initial position
         $('.search_results').html("");
         $('.container .logo').animate({
            marginTop: '15%',
            fontSize: 45
         }, 200);

         $('#search').animate({
            marginTop: '10%'
         }, 200, function()
         {
            $('.container .detailed').animate({
               fontSize: 16
            }, 200);
         });
      }
      else
      {
         $('#search').addClass('extended');

         $('.search_results').html("");
         
         $('.container .logo').animate({
            fontSize: 30
         }, 200);

         $('.container .logo_container').animate({
            marginTop: '5%',
         }, 200);

         $('.container .detailed').animate({
            fontSize: 12
         }, 200);

         $this.animate({
            marginTop: '5%'
         }, 200);

         search_by_title(sanit_searched_name, 1);
      }
   }
   else
   {
      if ($this.val() == "")
      {
         // ... return to initial position
         $('.search_results').html("");
         $('.container .logo_container').animate({
            marginTop: '15%',
         }, 200);

         $('.container .logo').animate({
            fontSize: 45
         }, 200);

         $('.container .detailed').animate({
            fontSize: 16
         }, 200);

         $('#search').animate({
            marginTop: '10%'
         }, 200, function()
         {
            $('#search').removeClass('extended');
         });
      }
      else
      {
         search_by_title(sanit_searched_name, 1);
      }
   }
}
