<?php
   ini_set('display_errors', 1);
   ini_set('display_startup_errors', 1);
   error_reporting(E_ALL);

   $DOCUMENT_ROOT = $_SERVER["DOCUMENT_ROOT"];

   // includes
   include_once($DOCUMENT_ROOT.'/functions.php');

   $value = filter_input(INPUT_GET, 'value');
   $page = filter_input(INPUT_GET, 'page');

   $api_key = "395d041f95c63722560dd9746ddfbda4";

   if (empty($value))
   {
      die(json_encode([
         "status" => "ko",
         "message" => "Problems with passed parameters, try again."
      ]));
   }

   $url = "https://api.themoviedb.org/3/search/movie";
   $parameters = [
      "api_key" => $api_key,
      "query" => $value,
      "page" => $page,
      "include_adult" => 1
   ];

   $search_results = api_call("GET", $url, $parameters);
   $search_results = json_decode($search_results, true);

   $temp = $search_results["results"];
   usort($temp, function($a, $b) {
      return $b["popularity"] <=> $a["popularity"];
   });

   $search_results["results"] = $temp;

   foreach ($search_results["results"] as $i => $s_r)
   {
      $poster_name = get_image_name($s_r["original_title"], $s_r["id"]);
      $search_results["results"][$i]["poster_name"] = $poster_name;
   }

   die(json_encode([
      "status" => "ok",
      "data" => $search_results
   ]));
