<?php

   function get_http_response_code($url) 
   {
      $headers = get_headers($url);
      return substr($headers[0], 9, 3);
   }

   function api_call($method, $url, $data = false)
   {
      $curl = curl_init();

      switch ($method)
      {
         case "POST":
            curl_setopt($curl, CURLOPT_POST, 1);

            if ($data)
               curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
            break;
         case "PUT":
            curl_setopt($curl, CURLOPT_PUT, 1);
            break;
         default:
            if ($data)
               $url = sprintf("%s?%s", $url, http_build_query($data));
      }

      curl_setopt($curl, CURLOPT_URL, $url);
      curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);

      $result = curl_exec($curl);

      curl_close($curl);

      return $result;
   }

   function isMobileDevice() 
   { 
      return preg_match("/(android|avantgo|blackberry|bolt|boost|cricket|docomo 
      |fone|hiptop|mini|mobi|palm|phone|pie|tablet|up\.browser|up\.link|webos|wos)/i" 
      , $_SERVER["HTTP_USER_AGENT"]); 
   }

   function get_image_name($original_title, $id)
   {
      return str_replace([" ", "/"], ["-", "-"], strtolower($original_title))."-".$id;
   }
